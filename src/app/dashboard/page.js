// `app/shivam/page.js` is the UI for the `/shivam` URL
import Link from 'next/link';

export default function Page() {
    return (
        <>
            <h1>Hello, Dashboard Page!</h1>
            <Link href="/shivam">Go to Shivam Page</Link>
        </>
    );
}
